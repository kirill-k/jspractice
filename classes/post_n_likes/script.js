let posts = [];

document.addEventListener('DOMContentLoaded', () => {

    class Post {
        constructor (title, text){
            this.id = Math.round(Math.random() * 10000);
            this.title = title;
            this.text = text;
            this.likes = 0;
            posts.push(this)

            this.addLikes = this.addLikes.bind(this);
        }

        addLikes(){
            console.log(this)
            this.likes++;
            this.render();
            // let node = document.querySelector(`.node[data-id="${this.id}"] > button`);
            // console.log(node)
            // node.innerHTML = `Likes ${this.likes}`;
        }

        render(){
            const nodeInDOM = document.querySelector(`.node[data-id="${this.id}"]`);
            const root = document.getElementById('root');
            let node;

            if ( nodeInDOM !== null ){
                node = nodeInDOM;
            } else {
                node = document.createElement('div');
            }

            console.log(nodeInDOM)


            node.innerHTML = `
                <div class="node" data-id="${this.id}">
                    <h1>${this.title}</h1>
                    <p>${this.text}</p>
                    <button class="likeBtn">Like ${this.likes}</button>
                </div>
            `;

            let likeBtn = node.querySelector('.likeBtn');
            likeBtn.addEventListener('click', this.addLikes);

            if ( nodeInDOM == null ){
                root.appendChild(node)
            }
        }
    }

    const post1 = new Post('this is a title1', 'and this is a text text text');
    const post2 = new Post('this is a title2', 'and this is a text text lalalalalalala');


    posts.map( item => item.render() )

});